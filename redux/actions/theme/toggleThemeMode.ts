export const type = 'toggleThemeMode';

const toggleThemeMode = () => {
    return {
        type,
    };
};
export default toggleThemeMode;