export const type = 'resetToaster';

const resetToaster = () => {
    return {
        type,
    };
};
export default resetToaster;