export interface AppLauncherDTO {
    link: string,
    icon: string;
    name: string;
    description: string;
}
